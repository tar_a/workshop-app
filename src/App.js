import React, { Component } from 'react';
import { BrowserRouter, Route ,Switch} from 'react-router-dom'
import Dashboard from './Components/Containers/Dashboard';
import Member from './Components/Containers/Member';
import Layouts from './Components/Views/Layouts';
import Login from './Components/Containers/Login';
import PrivateRoute from './Components/Containers/Authen/PrivateRoute';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={PrivateRoute(Layouts(Dashboard))} />
          <Route path="/members" component={PrivateRoute(Layouts(Member))} />
          <Route path="/login" component={Login} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;

