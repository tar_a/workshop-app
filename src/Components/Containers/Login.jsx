import React, { Component } from 'react'
import LoginForm from '../Views/LoginForm';
import Axios from 'axios';

export default class Login extends Component {
    state = {
        username: 'admin',
        password: '12345678',
        messageError:''
    }
    onClickLogin = () => {
        let { username, password } = this.state
        let data = {username,password}
        Axios.post('http://localhost:5555',data).then(response => {
            localStorage.setItem('token',response.data.token)
            this.props.history.push('/')
        }).catch( err => {
            this.setState({
                messageError:'invalid username or password'
            })
        })
    }
    onChange = (event) => {
        let {name,value} = event.target 
        this.setState({
            [name]:value
        })
    }
    render() {
        return (
            <div>
                <LoginForm data={this.state} onChange={this.onChange} onClickLogin={this.onClickLogin}/>
            </div>
        )
    }
}