import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
export default (Component) => {

    return class PrivateRoute extends Component {
        render() {
            let condition = localStorage.getItem('token') === "11Tdi2Br32sSezOI5s"
            return (condition) ?
                <Component />
                :
                <Redirect to="/login" />
        }
    }

}
