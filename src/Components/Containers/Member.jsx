import React, { Component } from 'react'
import ShowAllMember from '../Views/ShowAllMember';
import Axios from '../../../node_modules/axios';
import CreateMember from '../Views/CreateMember';

export default class Member extends Component {
  headers = {
    token: localStorage.getItem('token')
  }
  state = {
    users: [],
    onClickCreate: false,
    dataCreate: {
      name: '',
      phone: '',
      age: '',
      address: ''
    }
  }
  getAllMembers = () => {
    Axios.get('http://localhost:5555/users', { headers: this.headers }).then(response => {
      this.setState({
        users: response.data
      })
    })
  }
  componentDidMount() {
    this.getAllMembers()
  }
  onClickRemoveMember = (id) => {
    Axios.delete(
      `http://localhost:5555/users/${id}`,
      { headers: this.headers })
      .then(response => {
        this.getAllMembers()
      })
  }
  onClickToCreate = () => {
    this.setState({
      onClickCreate: true
    })
  }
  onChangeInputCreate = (e) => {
    let { name, value } = e.target
    this.setState({
      dataCreate: {
        ...this.state,
        [name]: value
      }
    })
  }
  onClickCreate = () => {
    let data = this.state.dataCreate
    Axios.post(
      `http://localhost:5555/users`,data,
      { headers: this.headers })
      .then(response => {
        this.getAllMembers()
        this.onClickBack()
      })
  }
  onClickBack = () => {
    this.setState({
      onClickCreate: false
    })
  }
  render() {
    return (
      <div className="center">
        <button onClick={this.onClickToCreate}>create</button>

        {
          (this.state.onClickCreate) ?
            <div>
              <button onClick={this.onClickBack}>back</button>
              <CreateMember data={this.state.dataCreate} onChange={this.onChangeInputCreate} onClickCreate={this.onClickCreate} />

            </div>
            :
            <ShowAllMember onClickRemoveMember={this.onClickRemoveMember} users={this.state.users} />

        }
      </div>
    )
  }
}
