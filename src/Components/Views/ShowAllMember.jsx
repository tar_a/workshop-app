import React from 'react'

export default ({ users , onClickRemoveMember }) => {
  return (
    <div className="center">
      {
        users.map((value, index) => {
          let { id, name, age, phone } = value
          return (
            <div key={index}>
              <li className="member" >
                {`${id} ${name} ${age} ${phone}`}
              </li>
              <button  onClick={() => onClickRemoveMember(id)}>romove</button>
            </div>
          )
        })
      }
    </div>
  )
}
