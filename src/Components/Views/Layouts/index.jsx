import React, { Component } from 'react'
import Sidebar from './Sidebar';
import Header from './Header';

export default (Component) => {

    return class Layout extends Component {
        render() {
            return (
                <div>
                    <Header/>
                    <div className="sidebar-layout">
                        <Sidebar />
                    </div>
                    <Component />
                </div>
            )
        }
    }

}
