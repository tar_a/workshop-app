import React from 'react'
import { NavLink } from 'react-router-dom'
export default () => {
    return (
        <div className="sidebar">
            <NavLink
                exact
                className="nav-link"
                activeClassName="nav-link-active"
                to='/'>DASHBOARD
            </NavLink>
            <NavLink
                className="nav-link"
                activeClassName="nav-link-active"
                to='/members'>MEMBERS
            </NavLink>
        </div>
    )
}
