import React from 'react'
import {withRouter} from 'react-router-dom'
let Header =  ({history}) => {
  let onClickLogOut = () => {
    localStorage.removeItem('token')
    history.push('/login')
  }
  return (
    <div className="header">
        <button onClick={onClickLogOut} className="btn-logout">Logout</button>
    </div>
  )
}

export default withRouter(Header)
