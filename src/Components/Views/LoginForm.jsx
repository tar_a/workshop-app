import React from 'react'

export default ({onClickLogin,onChange,data:{username,password,messageError}}) => {
    return (
        <div className="center loginform">
            <label htmlFor="" className="loginform-error-message">{messageError}</label>
            <input name="username" className="loginform-input" onChange={onChange} type="text" value={username} />
            <input name="password" className="loginform-input" onChange={onChange} type="password" value={password} />
            <button className="loginform-btn" onClick={onClickLogin} >Login</button>
        </div>
    )
}
