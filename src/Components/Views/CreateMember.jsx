import React from 'react'

export default ({data,onChange,onClickCreate}) => {
    let {name,phone,age,address} = data
  return (
    <div className="center loginform">
            <input name="name" className="loginform-input" onChange={onChange} type="text" value={name} />
            <input name="phone" className="loginform-input" onChange={onChange} type="text" value={phone} />
            <input name="address" className="loginform-input" onChange={onChange} type="text" value={address} />
            <input name="age" className="loginform-input" onChange={onChange} type="text" value={age} />
            <button className="loginform-btn" onClick={onClickCreate} >Create</button>
        </div>
  )
}
